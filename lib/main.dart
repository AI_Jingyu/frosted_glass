import 'package:flutter/material.dart';
import 'frosted_glass.dart';

void main()=>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Frosted glass UI',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
        body: FrostedClassDemo(),
      ),
    );
  }
}