import 'package:flutter/material.dart';
import 'dart:ui';

class FrostedClassDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Frosted Glass')),
      body: Stack(
        children: <Widget>[
          ConstrainedBox(
            //add extra limitation to the child in widget
            constraints: const BoxConstraints.expand(),
            child: Image.network('https://gss3.bdstatic.com/7Po3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike150%2C5%2C5%2C150%2C50/sign=ecb81c924036acaf4ded9eae1db0e675/9825bc315c6034a87988e72ac013495409237636.jpg'),
            
          ),
          Center(
            child: ClipRect(
              //put a rectangle on the pic above to make it looks like frosted glass
              child: BackdropFilter(
                // background filter
                filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                child: Opacity(
                  // make the rectangle transparent so that can see the pic under the filter
                  opacity: 0.5,
                  child: Container(
                    width: 500.0,
                    height: 500.0,
                    decoration: BoxDecoration(color: Colors.grey.shade200),
                    child: Center(
                      child: Text(
                        'Frosted glass',
                        style: Theme.of(context).textTheme.display3,
                        ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}